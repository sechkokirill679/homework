#include <iostream>
using namespace std;

void randomArray(int(&arr)[10], int);
void displayArray(int[], int);
void enterArray(int(&arr)[10], int);
void typeOfSequence(int[], int);

enum ratio : unsigned char
{
	less = 4,
	equals = 2,
	more = 1
};

int main()
{
	const int m = 10;
	int set[m]{ 0 };
	enterArray(set, m);
	displayArray(set, m);
	typeOfSequence(set, m);
}

void displayArray(int numbers[], int n)
{
	for (size_t i = 0; i < n; i++)
	{
		cout << numbers[i] << " | ";
	}
	cout << endl;
}

void randomArray(int(&numbers)[10], int n)
{
	srand(time(NULL));
	for (int i = 0; i < n; i++)
	{
		numbers[i] = -500 + rand() % 1000;
	}
}

void typeOfSequence(int set[], int n)
{
	char flag = 0;
	for (size_t i = 1; i < n; i++)
	{
		if (set[i - 1] == set[i])
		{
			flag = flag | ratio::equals;
		}
		if (set[i - 1] > set[i])
		{
			flag = flag | ratio::less;
		}
		if (set[i - 1] < set[i])
		{
			flag = flag | ratio::more;
		}
	}

	switch (flag)
	{
	case ratio::equals:
		cout << "Monotonic";
		break;
	case ratio::less:
		cout << "Strictly decreasing";
		break;
	case ratio::more:
		cout << "Strictly increasing";
		break;
	case ratio::more | ratio::equals:
		cout << "Increasing";
		break;
	case ratio::less | ratio::equals:
		cout << "Decreasing";
		break;
	default:
		cout << "Unsorted";
		break;
	}
}

void enterArray(int(&set)[10], int m)
{
	int n = 0; //  10 ,         Unsorted
	cout << "Enter the amount of elements less than " << m << endl;
	cin >> n;
	cout << "Please enter value of each element: " << endl;
	for (size_t i = 0; i < n; i++)
	{
		int tmp;
		cin >> tmp;
		set[i] = tmp;
	}
}
