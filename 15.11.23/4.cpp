#include <iostream>
using namespace std;

int FibonachiNumber(int n, int first, int second);

int main()
{
    int n = 0;
    cout << "Enter the number of Fibonachi sequence: ";
    cin >> n;
    cout << FibonachiNumber(n, 0, 1);
}

int FibonachiNumber(int n, int first, int second)
{
    if (n == 2)
    {
        return second;
    }
    return FibonachiNumber(n - 1, second, second + first);
}
