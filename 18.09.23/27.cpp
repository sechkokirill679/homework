#include <iostream>
#include <cmath>

using namespace std;
int main()
{
	int x;
	cout << "Enter x : ";
	cin >> x;
	if (x < 0) {
		cout << "No solutions";
		return 0;
	}
	else if (x % 2 == 0) {
		cout << "f(" << x << ")= " << 1;
	}
	else if (x % 2 != 0) {
		cout << "f(" << x << ")= " << -1;

	}
	return 0;
}
