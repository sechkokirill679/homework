#include <iostream>
#include <cmath>

using namespace std;
int main()
{
	unsigned int x;
	cout << "Enter number of year : ";
	cin >> x;
	if (x % 4 == 0 && (x % 100 != 0 || x % 400 == 0)) {
		cout << x << " is a leap year";

	}
	else cout << x << " is a not leap year";

	return 0;
}
