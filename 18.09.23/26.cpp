#include <iostream>
#include <cmath>

using namespace std;
int main()
{
	double x;
	cout << "Enter x : ";
	cin >> x;
	if (x <= 0) {
		cout << "f(" << x << ")= " << -x;

	}
	else if (x > 0 && x < 2) {
		cout << "f(" << x << ")= " << pow(x, 2);
	}
	else if (x >= 2) {
		cout << "f(" << x << ")= " << 4;

	}
	return 0;
}

