#include <iostream>
#include <cmath>

using namespace std;
int main()
{
	int x;
	cout << "Enter number: ";
	cin >> x;
	if (x > 0 && x % 2 == 0) {
		cout << x << " is a positive even number";

	}
	else if (x > 0 && x % 2 != 0) {
		cout << x << " is a positive odd number";

	}
	else if (x < 0 && x % 2 == 0) {
		cout << x << " is a negative even number";

	}
	else if (x < 0 && x % 2 != 0) {
		cout << x << " is a negative odd number";

	}
	else if (x == 0) {
		cout << x << "zero number";

	}

	return 0;
}
