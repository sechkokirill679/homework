#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double fact = 1, sum_n = 0, i = 1, n, sum = 0;
    cout << "Enter n: ";
    cin >> n;
    while (i <= n) {
        fact *= i;
        sum_n += 1/i;
        sum += fact / sum_n;
        cout << fact << "/" << sum_n << endl;
        i++;
    }
    cout << sum;
}
