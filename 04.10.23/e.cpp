#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    double i{ 1 }, n, sum = 0;
    cout << "Enter n: ";
    cin >> n;
    while (i <= n) {
        cout << "1/" << pow(2 * i + 1, 2) << endl;
        sum += 1 / pow(2 * i + 1, 2);
        i++;
    }
    cout << sum;
}
