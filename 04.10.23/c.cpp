#include <iostream>
#include <cmath>
using namespace std;

int main()
{
    unsigned int k;
    double i{ 1 }, n, sum = 0;
    cout << "Enter n: ";
    cin >> n;
    cout << "Enter k: ";
    cin >> k;
    while (i <= n) {
        cout << "1/" << pow(i, k) << endl;
        sum += 1 / pow(i, k);
        i++;
    }
    cout << sum;
}
