#include <iostream>
using namespace std;

enum Allergens : unsigned char {
	eggs = 1,
	peanuts = 2,
	shellfish = 4,
	strawberries = 8,
	tomatoes = 16,
	chocolate = 32,
	pollen = 64,
	cats = 128
};

bool isAllegricTo(int, Allergens);
void allergensList(int);

int main() {
	int allergyTest;
	cout << "Enter number to check the allergy: ";
	cin >> allergyTest;
	allergensList(allergyTest);
}
void allergensList(int allergyTest) {
	if (isAllegricTo(allergyTest, Allergens::eggs))
		cout << "eggs" << endl;
	if (isAllegricTo(allergyTest, Allergens::peanuts))
		cout << "peanuts" << endl;
	if (isAllegricTo(allergyTest, Allergens::shellfish))
		cout << "shellfish" << endl;
	if (isAllegricTo(allergyTest, Allergens::strawberries))
		cout << "strawberries" << endl;
	if (isAllegricTo(allergyTest, Allergens::tomatoes))
		cout << "tomatoes" << endl;
	if (isAllegricTo(allergyTest, Allergens::chocolate))
		cout << "chocolate" << endl;
	if (isAllegricTo(allergyTest, Allergens::pollen))
		cout << "pollen" << endl;
	if (isAllegricTo(allergyTest, Allergens::cats))
		cout << "cats" << endl;
}
bool isAllegricTo(int allergyTest, Allergens allergen) {
	return allergyTest & allergen;
}
