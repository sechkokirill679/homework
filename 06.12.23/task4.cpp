#include <iostream>

using namespace std;

void getUserString(char arr[], int);
void compareTwoStrings(char arr1[], char arr2[]);

int main()
{
    const int maxLength = 100;
    char text1[maxLength]{};
    getUserString(text1, maxLength);
    char text2[maxLength]{};
    getUserString(text2, maxLength);

    compareTwoStrings(text1, text2);
}

void getUserString(char arr[], int maxLength)
{
    cout << "Enter some text, less than " << maxLength << " symbols: ";
    cin.getline(arr, maxLength);
}

void compareTwoStrings(char arr1[], char arr2[])
{
    int value1 = 0;
    for (size_t i = 0; arr1[i]; i++)
    {
        value1 += static_cast<int>(arr1[i]);
    }

    int value2 = 0;
    for (size_t i = 0; arr2[i]; i++)
    {
        value2 += static_cast<int>(arr2[i]);
    }

    if (value1 < value2)
    {
        cout << arr1 << " less than " << arr2;
    }
    else if (value1 == value2)
    {
        cout << arr1 << " euals " << arr2;
    }
    else
    {
        cout << arr1 << " more " << arr2;
    }
}
