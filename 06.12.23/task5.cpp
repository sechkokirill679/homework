#include <iostream>

using namespace std;

int firstEntrance(char arr1[], char arr2[]);

int main()
{
    const int maxLength = 100;
    char string[maxLength]{};
    cout << "Enter some string, less than " << maxLength << " symbols: ";
    cin.getline(string, maxLength);

    char substring[maxLength]{};
    cout << "Enter some string to find entrance, less than " << maxLength << " symbols: ";
    cin.getline(substring, maxLength); 

    try
    {
        int entranceIndex = firstEntrance(string, substring);
        cout << entranceIndex;
    }
    catch (...)
    {
        cout << "Error: substring isn't into string";
    }

}

int firstEntrance(char string[], char substring[])
{
    int entranceIndex = 0;
    bool isEnter = false;
    for (size_t i = 0; string[i]; i++)
    {
        for (size_t j = 0; substring[j]; j++)
        {
            if (substring[j] == string[i + j])
            {
                isEnter = true;
                entranceIndex = i;
                if (!substring[j + 1])
                {
                    return entranceIndex;
                }
            }
            else
            {
                isEnter = false;
                entranceIndex = 0;
                break;
            }
        }
    }

    throw "Substring isn't in string";
}
