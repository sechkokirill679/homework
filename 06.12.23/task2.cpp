#include <iostream>

using namespace std;

int enterArray(int arr[], int);
void displayArray(int arr[], int);
int checkArray(int arr[], int, int);
int getUserNumber();

int main()
{
	const int length = 30;
	int numbers[length]{ 0 };
	int newLength = enterArray(numbers, length);
	displayArray(numbers, newLength);
	int result = checkArray(numbers, newLength, getUserNumber());
	cout << result;
}

int getUserNumber()
{
	int number = 0;
	cout << "Please, enter number to check: ";
	cin >> number;
	return number;
}

int enterArray(int arr[], int length)
{
	int newLength = 0;
	cout << "Enter numbers less than " << length << ": ";
	cin >> newLength;
	for (size_t i = 0; i < newLength; i++)
	{
		cout << "Enter " << i + 1 << " element: ";
		cin >> arr[i];
	}
	return newLength;
}

void displayArray(int numbers[], int length)
{
	for (size_t i = 0; i < length; i++)
	{
		cout << numbers[i] << " ";
	}
	cout << "\n";
}

int checkArray(int numbers[], int length, int num)
{
	int sum = 0;
	for (size_t i = 0; i < length; i++)
	{
		if (numbers[i] < num)
		{
			sum += 1;
		}
	}
	return sum;
}
