#include <iostream>

using namespace std;

int enterArray(int arr[], int);
void displayArray(int arr[], int);
void reverseArray(int arr[], int);

int main()
{
	const int length = 30;
	int numbers[length]{ 0 };
	int newLength = enterArray(numbers, length);
	displayArray(numbers, newLength);
	reverseArray(numbers, newLength);
	displayArray(numbers, newLength);
}

void reverseArray(int arr[], int length)
{
	for (size_t i = 0; i < length / 2; i++)
	{
		int tmp = arr[i];
		arr[i] = arr[length - i - 1];
		arr[length - i - 1] = tmp;
	}
}

int enterArray(int arr[], int length)
{
	int newLength = 0;
	cout << "Enter numbers less than " << length << ": ";
	cin >> newLength;
	for (size_t i = 0; i < newLength; i++)
	{
		cout << "Enter " << i + 1 << " element: ";
		cin >> arr[i];
	}
	return newLength;
}

void displayArray(int numbers[], int length)
{
	for (size_t i = 0; i < length; i++)
	{
		cout << numbers[i] << " ";
	}
	cout << "\n";
}
